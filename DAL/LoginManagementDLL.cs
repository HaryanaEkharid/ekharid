﻿using BOL.eKharid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.eKharid
{
  public  class LoginManagementDLL
    {
        public DataSet CheckLogin(LoginManagementEntity _loginEntity)
        {
            SqlParameter[] sqlParameterArray = new SqlParameter[4]
            {
             new SqlParameter("@UserName", (object) _loginEntity.Username),
             new SqlParameter("@Password", (object) _loginEntity.Pasword),
             new SqlParameter("@UserType", (object) _loginEntity.UserType),
             new SqlParameter("@DeviceId", (object) _loginEntity.DeviceId),
             
            };
            return SqlHelper.ExecuteDataset(SqlHelper.CONN_STRING, CommandType.StoredProcedure, "usp_userLogin", sqlParameterArray);
        }
    }
}
