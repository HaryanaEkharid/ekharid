﻿using BAL.eKharid;
using BOL.eKharid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eKharid_UI
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Response.Expires = -1;

            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            Response.Expires = -1500;
            Response.CacheControl = "no-cache";
            Response.Cache.SetNoStore();
            string browser = Request.Browser.Browser;
            if (browser == "IE")
            {
                Response.CacheControl = "No-Cache";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }
            else
            {
                Response.Cache.SetAllowResponseInBrowserHistory(false);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Expires = -1;
                Response.Cache.SetNoStore();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }

            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (Session["LIn"] != null && Session["AuthToken"] != null
                               && Request.Cookies["AuthToken"] != null)
            {
                if (!Session["AuthToken"].ToString().Equals(
                               Request.Cookies["AuthToken"].Value))
                {
                    Response.Write("<Script>alert('You are not logged in!')</Script>");

                    return;
                }
            }
            if (!Page.IsPostBack)
            {
               CommonFunction.AntiFixationInit();
               CommonFunction.AntiHijackInit();
                clearText();
            }

       }
        private void clearText()
        {
            txtUserName.Value = string.Empty;
            txtPassword.Text = string.Empty;
            
        }
       protected void btnLogin_Click(object sender, EventArgs e)
        {

            if (Request.HttpMethod.ToString().ToUpper() == "POST")
            {
                if (Page.IsValid)
                {
                    if (Convert.ToString(Session["captcha"]).ToLower() != txtVerficationCode.Text.Trim().ToLower())
                    {

                        String st = "<script language='javascript' type=text/javascript>alert('Invalid Captcha Code')</script>";
                        ClientScript.RegisterStartupScript(GetType(), "popup", "" + st + "");
                        txtVerficationCode.Focus();
                        return;
                    }
                    try
                    {
                        if (txtUserName.Value.Trim() == "")
                        {
                            String st = "<script language='javascript' type=text/javascript>alert('Invalid UserName/Password')</script>";
                            ClientScript.RegisterStartupScript(GetType(), "popup", "" + st + "");
                            txtUserName.Focus();
                            return;
                        }
                        if (txtPassword.Text.Trim() == "")
                        {
                            String st = "<script language='javascript' type=text/javascript>alert('Invalid UserName/Password')</script>";
                            ClientScript.RegisterStartupScript(GetType(), "popup", "" + st + "");
                            txtPassword.Focus();
                            return;
                        }
                        string struname, strpass;
                        struname = hdUserName.Value;
                        strpass = hdPassword.Value;

                        DataSet _dslogin = null;

                        LoginManagementBLL _objLoginBLL = new LoginManagementBLL();
                        LoginManagementEntity _objLoginEntity = new LoginManagementEntity();
                        _objLoginEntity.Username = CommonFunction.DecryptStringAES(struname);
                        _objLoginEntity.Pasword = CommonFunction.DecryptStringAES(strpass);
                        _dslogin = _objLoginBLL.CheckLogin(_objLoginEntity);
                        if (_dslogin.Tables[0].Rows.Count > 0)
                        {
                            string guid1 = Guid.NewGuid().ToString();
                            Session.Add("AuthToken", guid1);
                            Response.Cookies.Add(new HttpCookie("AuthToken", guid1));
                            Session["usr"] = struname;
                            string _uType = Convert.ToString(_dslogin.Tables[0].Rows[0]["RoleId"]);
                            Session["uRole"] = _uType;

                            switch (_uType)
                            {
                                case "2":
                                         Response.Redirect("dashboard.aspx", false);

                                         break;
                                default:
                                         Response.Redirect("login.aspx", false);
                                         break;

                            }
                        }
                        else
                        {
                            String st = "<script language='javascript' type=text/javascript>alert('Invalid UserName/Password')</script>";
                            ClientScript.RegisterStartupScript(GetType(), "popup", "" + st + "");
                            txtPassword.Focus();

                        }

                    }
                    catch (System.Threading.ThreadAbortException)
                    {
                        // ignore it
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }
    }
}