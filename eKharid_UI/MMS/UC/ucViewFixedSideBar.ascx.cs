﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
public partial class Admin_UC_ucViewFixedSideBar : System.Web.UI.UserControl
{
    public string menu = string.Empty;
    Int32 _submenucount = 0;
    public System.Text.StringBuilder strMenu = new System.Text.StringBuilder();
    public string strCollegeMenu = "";
    AdminManagement.UserMenu page = new AdminManagement.UserMenu();
    //OfficeManagement.Users localUserData = new OfficeManagement.Users();
    private DataTable localUserData;
    List<List<AdminManagement.UserMenu>> objPage = new List<List<AdminManagement.UserMenu>>();
    int seq = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        localUserData = (DataTable)Session["LoginUserData"];
        if (localUserData != null)
        {
            if (localUserData.Rows.Count > 0)
            {
                page.UserName = localUserData.Rows[0]["UserID"].ToString();//Convert.ToString(Session["username"]);
                page.FkCompanyCode = MyLib.GetCompanyCode();
                objPage = page.FetchUserMenuList(0, page);
                BindParentModule(objPage[0], objPage[1]);
            }
        }
    }

    void BindParentModule(List<AdminManagement.UserMenu> Modulelist, List<AdminManagement.UserMenu> SubModulelist)
    {
       // string imgUrl = localUserData.DisplayPicture != null ? "../Upload/" + localUserData.DisplayPicture : "assets/img/find_user.png";
        string imgUrl = localUserData.Rows[0]["DisplayPic"] != null ? "../Upload/" + localUserData.Rows[0]["DisplayPic"].ToString() : "assets/img/find_user.png";
        strMenu.Append("<ul class='nav' id='main-menu'><li style='margin-bottom: 4px;' class='text-center'><img src=" + imgUrl + " class='user-image img-responsive'/><lable style='color:#fff;'>" + localUserData.Rows[0]["DisplayName"].ToString() + "</lable></li><li><a class='active-menu' onclick=LoadForm('Default.aspx') href='javascript:void(0);'><i class='fa fa-dashboard fa-3x'></i>Dashboard</a></li>");
        for (int i = 0; i < Modulelist.Count; i++)
        {
            strMenu.Append("<li><a href='javascript:void(0);'><i class='"+Modulelist[i].CssClass+"'></i>" + Modulelist[i].ModuleName + "<span class='fa arrow'></span></a>");
            if (SubModulelist.Exists(x => x.ParentModuleID == Modulelist[i].ModuleID))
            {
                List<AdminManagement.UserMenu> filteredList = SubModulelist.Where(x => x.ParentModuleID == Modulelist[i].ModuleID).ToList();
                BindSubModule(filteredList, Convert.ToInt32(Modulelist[i].ModuleID));
            }
            else
            {
             //   List<AdminManagement.UserMenu> filteredList = objPage[2].Where(x => x.ModuleID == SubModulelist[i].ModuleID).ToList();
             //   BindForm(filteredList, Convert.ToInt32(SubModulelist[i].ModuleID));
            }
            strMenu.Append("</li>");
        }
        strMenu.Append("</ul>");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="SubModulelist"></param>
    /// <param name="ModuleID"></param>
    void BindSubModule(List<AdminManagement.UserMenu> SubModulelist,int ModuleID)
    {
        strMenu.Append("<ul class='nav nav-second-level'>");
        for (int i = 0; i < SubModulelist.Count; i++)
        {
            strMenu.Append("<li><a href='javascript:void(0);'>" + SubModulelist[i].ModuleName + "<span class='fa arrow'></span></a>");
            List<AdminManagement.UserMenu> filteredList = objPage[2].Where(x => x.ModuleID == SubModulelist[i].ModuleID).ToList();
            BindForm(filteredList, Convert.ToInt32(SubModulelist[i].ModuleID));
        }
        strMenu.Append("</li></ul>");
    }
    void BindForm(List<AdminManagement.UserMenu> Formlist, int ModuleID)
    {
        strMenu.Append("<ul class='nav nav-third-level'>");
        for (int i = 0; i < Formlist.Count; i++)
        {
            if (Formlist[i].WriteMode=="1")
            {
                if (Formlist[i].EntryFormTitle.Contains("Add Achievements") || Formlist[i].EntryFormTitle.Contains("View Achievements"))
                {
                    strMenu.Append("<li><a onclick=LoadForm('" + Formlist[i].EntryFormUrl + "?R=" + Formlist[i].ReadMode + "&W=" + Formlist[i].WriteMode + "&D=" + Formlist[i].DeleteMode + "&Mode=1" + "') href='javascript:void(0);'>" + Formlist[i].EntryFormTitle + "</a></li>");
                }
                else
                {
                    // strMenu.Append("<li><a href='"+Formlist[i].EntryFormUrl +"?R="+Formlist[i].ReadMode+"&W="+Formlist[i].WriteMode+"&D="+Formlist[i].DeleteMode+"'>" + Formlist[i].EntryFormTitle + "</a></li>");
                    strMenu.Append("<li><a onclick=LoadForm('" + Formlist[i].EntryFormUrl + "?R=" + Formlist[i].ReadMode + "&W=" + Formlist[i].WriteMode + "&D=" + Formlist[i].DeleteMode + "') href='javascript:void(0);'>" + Formlist[i].EntryFormTitle + "</a></li>");
                    // strMenu.Append("<li><a onclick=LoadForm('AddUpdate-Menu.aspx') href='javascript:void(0);'> " + Formlist[i].EntryFormTitle + "</a></li>");
                }
            }
            if (Formlist[i].ReadMode == "1" || Formlist[i].DeleteMode == "1")
            {
                if (Formlist[i].EntryFormTitle.Contains("Add Achievements") || Formlist[i].EntryFormTitle.Contains("View Achievements"))
                {
                    strMenu.Append("<li><a onclick=LoadForm('" + Formlist[i].ListFormUrl + "?R=" + Formlist[i].ReadMode + "&W=" + Formlist[i].WriteMode + "&D=" + Formlist[i].DeleteMode + "&Mode=1" + "') href='javascript:void(0);'>" + Formlist[i].ListFormTitle + "</a></li>");
                }
                else
                {
                    //strMenu.Append("<li><a href='" + Formlist[i].ListFormUrl + "?R=" + Formlist[i].ReadMode + "&W=" + Formlist[i].WriteMode + "&D=" + Formlist[i].DeleteMode + "'>" + Formlist[i].ListFormTitle + "</a></li>");
                    strMenu.Append("<li><a onclick=LoadForm('" + Formlist[i].ListFormUrl + "?R=" + Formlist[i].ReadMode + "&W=" + Formlist[i].WriteMode + "&D=" + Formlist[i].DeleteMode + "') href='javascript:void(0);'>" + Formlist[i].ListFormTitle + "</a></li>");
                }
                //strMenu.Append("<li><a onclick=LoadForm('AddUpdate-Menu.aspx') href='javascript:void(0);'>" + Formlist[i].EntryFormTitle + "</a></li>");
            }
        }
        strMenu.Append("</ul>");
    }
}