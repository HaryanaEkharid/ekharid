﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucViewFixedSideBar.ascx.cs"
    Inherits="Admin_UC_ucViewFixedSideBar" %>
<nav class="navbar-default navbar-side" role="navigation" id="navLeftMenu">
    <div class="sidebar-collapse">
   <%--     <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="assets/img/find_user.png" class="user-image img-responsive" />
            </li>
            <li>
                <a class="active-menu" href="Default.aspx"><i class="fa fa-dashboard fa-3x"></i>Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-3x"></i>Admin Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="#">Add Update Menu<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Menu.aspx">Add Menu</a>
                            </li>
                            <li>
                                <a href="View-Menu.aspx">View Menu</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Add Update QuickLink Group<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-QuickLinkGroup.aspx">Add QuickLink Group</a>
                            </li>
                            <li>
                                <a href="View-QuickLinkGroup.aspx">View QuickLink Group</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Configure Menu<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="Menu-Configuration.aspx">Configure Menu</a>
                            </li>
                            <li>
                                <a href="Menu-Configuration.aspx">View Configure Menu</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-3x"></i>Media Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="AddUpdate-BannerGroup.aspx">Add Banner Group</a>
                    </li>
                    <li>
                        <a href="View-BannerGroup.aspx">View Banner Group</a>
                    </li>

                    <li>
                        <a href="AddUpdate-Banners.aspx">Add Banners</a>
                    </li>
                    <li>
                        <a href="View-Banners.aspx">View Banners</a>
                    </li>

                    <li>
                        <a href="AddUpdate-Albums.aspx">Add Albums</a>
                    </li>
                    <li>
                        <a href="View-Albums.aspx">View Albums</a>
                    </li>

                    <li>
                        <a href="AddUpdate-Images.aspx">Add Images</a>
                    </li>
                    <li>
                        <a href="View-Images.aspx">View Images</a>
                    </li>
                    <li>
                        <a href="AddUpdate-Videos.aspx">Add Videos</a>
                    </li>
                    <li>
                        <a href="View-Videos.aspx">View Videos</a>
                    </li>
                    <li>
                        <a href="AddUpdate-VirtualTour.aspx">Add Virtual Tour</a>
                    </li>
                    <li>
                        <a href="View-VirtualTour.aspx">View Virtual Tour</a>
                    </li>
                     <li>
                        <a href="AddUpdate-Testimonials.aspx">Add Testimonials</a>
                    </li>
                    <li>
                        <a href="View-Testimonials.aspx">View Testimonials</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-qrcode fa-3x"></i>Web Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Add View Circulars<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Circulars.aspx">Add Circulars</a>
                            </li>
                            <li>
                                <a href="View-Circulars.aspx">View Circulars</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Events<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Events.aspx">Add Events</a>
                            </li>
                            <li>
                                <a href="View-Events.aspx">View Events</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="#">Add View News<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-News.aspx">Add News</a>
                            </li>
                            <li>
                                <a href="View-News.aspx">View News</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Achievements<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-News.aspx?Mode=1">Add Achievements</a>
                            </li>
                            <li>
                                <a href="View-News.aspx?Mode=1">View Achievements</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Press Releases<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-PressReleases.aspx">Add Press Releases</a>
                            </li>
                            <li>
                                <a href="View-PressReleases.aspx">View Press Releases</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Brochures<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Brochures.aspx">Add Brochures</a>
                            </li>
                            <li>
                                <a href="View-Brochures.aspx">View Brochures</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Notice Board<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-NoticeBoard.aspx">Add Notice Board</a>
                            </li>
                            <li>
                                <a href="View-NoticeBoard.aspx">View Notice Board</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="#">Add View Academic Calendar<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-AcademicCalendar.aspx">Add Academic Calendar</a>
                            </li>
                            <li>
                                <a href="View-AcademicCalendar.aspx">View Academic Calendar</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-qrcode fa-3x"></i>Office Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Add View Cabinet<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Cabinet.aspx">Add Cabinet</a>
                            </li>
                            <li>
                                <a href="View-Cabinet.aspx">View Cabinet</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Toppers<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Toppers.aspx">Add Toppers</a>
                            </li>
                            <li>
                                <a href="View-Toppers.aspx">View Toppers</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-qrcode fa-3x"></i>User Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Add View Module<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Module.aspx">Add Module</a>
                            </li>
                            <li>
                                <a href="View-Module.aspx">View Module</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Form<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Form.aspx">Add Form</a>
                            </li>
                            <li>
                                <a href="View-Form.aspx">View Form</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Profile<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Profile.aspx">Add Profile</a>
                            </li>
                            <li>
                                <a href="View-Profile.aspx">View Profile</a>
                            </li>
                            <li>
                                <a href="AddUpdate-ProfileRights.aspx">Assign Rights To Profile</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Add View Users<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="AddUpdate-Users.aspx">Add Users</a>
                            </li>
                            <li>
                                <a href="View-Users.aspx">View Users</a>
                            </li>
                            <li>
                                <a href="AddUpdate-UserRights.aspx">Assign Rights To Users</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

              <li>
                <a href="#"><i class="fa fa-qrcode fa-3x"></i>Client Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="#">View Request<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="ViewGetInTouch.aspx">View Request</a>
                            </li>                          
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>--%>
            <%=strMenu%>
    </div>
</nav>
