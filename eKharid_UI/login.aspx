﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="eKharid_UI.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <script type="text/javascript" src="js/aes.js"></script>
    <script type="text/javascript" src="js/enc-base64-min.js" ></script>
     <script type="text/javascript" language="javascript">
         function EncryptUsername() {
            
             if (document.getElementById('txtUserName').value != "") {

                 var text = document.getElementById('txtUserName').value;
                 
                 var key = CryptoJS.enc.Utf8.parse('8080808080808080');
                 
                 var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
                 var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse((text)), key,
                     {
                         keySize: 128/8,
                         iv: iv,
                         mode: CryptoJS.mode.CBC,
                         padding: CryptoJS.pad.Pkcs7
                     });
                 
                 document.getElementById('hdUserName').value = encrypted.toString();
                 document.getElementById('txtUserName').value = encrypted.toString();
             }
             return true;
         }
         function EncryptPassword() {
             if (document.getElementById('txtPassword').value != "") {
                 var text = document.getElementById('txtPassword').value;
                 var key = CryptoJS.enc.Utf8.parse('8080808080808080');
                 var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
                 var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse((text)), key,
                     {
                         keySize: 128 / 8,
                         iv: iv,
                         mode: CryptoJS.mode.CBC,
                         padding: CryptoJS.pad.Pkcs7
                     });
                 document.getElementById('hdPassword').value = encrypted.toString();
                 document.getElementById('txtPassword').value = encrypted.toString();
             }
             return true;
         }
         function DecryptData(obj) {
             var key = CryptoJS.enc.Base64.parse("8080808080808080");
             var iv = CryptoJS.enc.Base64.parse("8080808080808080");
             var decrypted = CryptoJS.AES.decrypt(obj, key, { iv: iv });
         }
     </script>
</head>
<body>
    <form id="form1" runat="server">
       <div class="main">
        <div class="login-form">
            <h1>Login</h1>
            <div class="head">
                <img alt="" src="../images/cada_logo.png" />
            </div>
           
                <div class="LoginConDiv">
                    <div class="form-group ">
                        <label for="exampleInputEmail1">
                            Username</label>
                        <%-- <input type="email" class="form-control formDiv" id="exampleInputEmail1" placeholder="Username">--%>
                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUserName"
                            Display="Dynamic" ErrorMessage="UserName cant be blank" ForeColor="Red" SetFocusOnError="True"
                            ValidationGroup="login">*</asp:RequiredFieldValidator>
                        <input type="text" id="txtUserName" runat="server" placeholder="Username" autocompletetype="None" autocomplete="off"  onchange="EncryptUsername();" />
                        <input type="hidden" id="hdUserName" runat="server" />

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Password</label>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                            Display="Dynamic" ErrorMessage="Password cant be blank" ForeColor="Red" SetFocusOnError="True"
                            ValidationGroup="login">*</asp:RequiredFieldValidator>
                        &nbsp;<asp:TextBox ID="txtPassword" runat="server" AutoCompleteType="None" autocomplete="off" placeholder="Password" TextMode="Password" onchange="EncryptPassword();"
                            class="form-control formDiv" ClientIDMode="Static"></asp:TextBox>
                        <input type="hidden" id="hdPassword" runat="server" />

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Verification Code</label>
                        <%--<asp:Label AssociatedControlID="txtVerficationCode" CssClass="loginLabel" runat="server"
                            ID="Label1">  </asp:Label>--%>
                        <asp:TextBox ID="txtVerficationCode" CssClass="form-control formDiv" Width="100" runat="server"
                            MaxLength="6" ondrop="false" onpaste="false" TabIndex="3" autocomplete="off"></asp:TextBox>
                        <asp:Image ID="Image2" Style="border-width: 0px;" ImageUrl="~/Captchaa.aspx" runat="server" />
                        <%--<img src='<%= this.GetImagePath() + "refresh-icon.png" %>' alt="refresh" onclick="refresh();" />--%>
                        <div class="cf">
                            <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator1" runat="server"
                                ErrorMessage="Please enter verification code" ControlToValidate="txtVerficationCode"
                                ValidationGroup="login"></asp:RequiredFieldValidator>
                        </div>
                        <div class="cf">
                            <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator2" runat="server"
                                ErrorMessage="Please enter verification code" ControlToValidate="txtVerficationCode"
                                ValidationGroup="ForgetPassowrd"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="p-container">
                    <label class="checkbox">
                      <%--  <input type="checkbox" checked="" name="checkbox"><i></i>Remember Me</label>--%>
                    <asp:Button ID="btnLogin" runat="server" Text="Login" class="button expand loginBut"
                         ValidationGroup="login" OnClick="btnLogin_Click" />
                    <div class="clear"></div>
                </div>

                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                    ForeColor="#CC0000" ShowMessageBox="True" ShowSummary="False" ValidationGroup="login" />
                    <div id="lkForgetPwd"><a style="color: Blue;" href="#">Forgot your password?</a></div>

                <div class="loginBox" id="divForgetPwd" style="display: none; width: 538px; height: 204px;">
                    <div style="top: 110px; left: 180px; position: absolute; display: none;" id="EmailSendingloder">
                        <img alt="Sending Email..." src="../images/EmailSending.png" />
                    </div>
                    <div class="login_inner">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td colspan="2" align="Center" valign="top" style="color: #486b91; font-size: 18px; font-style: italic; padding-top: 14px;">To Reset your Password, Enter your Email Address Below.
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <label type="label" style="margin-left: 10px; margin-top: 12px;">Email</label>
                                </td>

                                <td>
                                    <input id="txtemail" autocomplete="off" style="margin-right: -137px; margin-top: 22px;" cssclass="input" runat="server" />
                                   <label id="Label4" style="color: Red;">
                            </label>
                                </td>
                            </tr>



                            <tr>
                                <td>

                                    <button class="btn btn-medium login_btn" type="button" tabindex="26"  id="Button1"  onclick="forgetpwd();" style="float: right; margin-right: -312px;">
                                        Send</button>
                                    <button class="btn btn-medium btn-danger" tabindex="27" style="float: right; margin-right: -250px;"
                                        type="button" id="anchClose">
                                        Close</button>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

          
        </div>
        <!--//End-login-form-->
    </div>
    </form>
</body>
</html>
