﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
public partial class Captchaa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //System.Drawing.Bitmap objBmp = new System.Drawing.Bitmap(94,35);
        //System.Drawing.Graphics objGraphics = System.Drawing.Graphics.FromImage(objBmp);
        //objGraphics.Clear(System.Drawing.ColorTranslator.FromHtml("#45698d"));

        //objGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

        //System.Drawing.Font objFont = new System.Drawing.Font("#45698d", 16, System.Drawing.FontStyle.Bold);
        //string strRandom = "";
        //string[] strArray = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        //Random autoRand = new Random();
        //int x;
        //for (x = 0; x < 6; x++)
        //{
        //    int i = Convert.ToInt32(autoRand.Next(0, 36));
        //    strRandom += strArray[i].ToString();
        //}
        //Session.Add("strRandom", strRandom);
        //objGraphics.DrawString(strRandom, objFont, System.Drawing.Brushes.White, 3, 3);
        //Response.ContentType = "image/GIF";
        //objBmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
        //objFont.Dispose();
        //objGraphics.Dispose();
        //objBmp.Dispose();

        Bitmap bmp = new Bitmap(94, 35);
        Graphics g = Graphics.FromImage(bmp);

        g.Clear(Color.FromArgb(69, 105, 141));
        string randomString = GenerateCaptch.GetCaptchaString(6);
        Session.Add("captcha", randomString);

        g.DrawString(randomString, new System.Drawing.Font("#45698d", 16, System.Drawing.FontStyle.Bold), new SolidBrush(Color.WhiteSmoke), 2, 2);
        Response.ContentType = "image/jpeg";
        bmp.Save(Response.OutputStream, ImageFormat.Jpeg);
        bmp.Dispose();
    }
    public static class GenerateCaptch
    {
        public static string[] strArray = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

        public static string GetCaptchaString(int length)
        {
            string strRandom = "";
            //string[] strArray = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            Random autoRand = new Random();
            int x;
            for (x = 0; x < 6; x++)
            {
                int i = Convert.ToInt32(autoRand.Next(0, 36));
                strRandom += strArray[i].ToString();
            }

            return strRandom;
        }
    }
}