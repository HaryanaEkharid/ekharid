﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL.eKharid
{
   public class LoginManagementEntity
    {
        private string _username;
        public string Username
        {
            get
            {
                return this._username;
            }
            set
            {
                this._username = value;
            }
        }
        private string _password;
        public string Pasword
        {
            get
            {
                return this._password;
            }
            set
            {
                this._password = value;
            }
        }

        private int _userType;
        public int UserType
        {
            get
            {
                return this._userType;
            }
            set
            {
                this._userType = value;
            }
        }
        private string _deviceId;
        public string DeviceId
        {
            get
            {
                return this._deviceId;
            }
            set
            {
                this._deviceId = value;
            }
        }
        
    }
}
