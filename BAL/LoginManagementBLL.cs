﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOL.eKharid;
using DAL.eKharid;

namespace BAL.eKharid
{
  public  class LoginManagementBLL
    {
        private LoginManagementDLL _objLoginDLL;

        public DataSet CheckLogin(LoginManagementEntity _loginEntity)
        {
            this._objLoginDLL = new LoginManagementDLL();
            return this._objLoginDLL.CheckLogin(_loginEntity);
        }

    }
}
